package net.beanscode.bulk;

import static net.hypki.libs5.utils.args.ArgsUtils.exists;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import jodd.util.ArraysUtil;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.bulk.server.Server;
import net.beanscode.model.BeansConst;
import net.hypki.libs5.cli.noninteractivecli.CLICommand;
import net.hypki.libs5.cli.noninteractivecli.CLIManager;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.api.APIReturnType;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.utils.collections.ArrayUtils;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.StringUtilities;

public class BeansBulkMain {
	public static void main(String[] args) throws IOException, ValidationException {
		// logger
		if (exists(args, "verbose")	|| exists(args, "debug"))
			System.setProperty("log4j", "log4j-debug.properties");
		else
			System.setProperty("log4j", "log4j-client.properties");
		final String[] args2 = (String[]) ArrayUtils.concat(args, new String[] {"--log4j", System.getProperty("log4j")});
				
		System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");
		Watch buildingManager = new Watch();
		CLIManager manager = new CLIManager()
									.addCommand(Server.class)
//									.addCommand(Web.class)
									.addCommand(ApiCallCommand.class)
//									.addCommand(BeansClientMain.class)
//									.addCommand(ApiCallCommand.class)
//									.searchForCLIClasses(Server.class)
//									.searchForCLIClasses(BeansClientMain.class)
//									.searchForCLIClasses(BeansServerMain.class)
									;
		LibsLogger.debug(BeansBulkMain.class, "Building manager in ", buildingManager.toString());
		
		if (StringUtilities.nullOrEmpty(manager.extractName(args2))) {
			
			// if no command is specified, start server and web interface
			
			// start server
			new Thread(){ 
				@Override
				public void run() {
					try {
						// starting some beans services 
						BeansConst.init(args2);
						
						// default: server --settings beans-dev.json --db mapdb
						String[] newArgs = args2;
						newArgs = ArraysUtil.insert(newArgs, new String[]{"server"}, 0);
						if (!ArgsUtils.exists(newArgs, "settings")) {
							newArgs = ArraysUtil.append(newArgs, "--settings");
							newArgs = ArraysUtil.append(newArgs, "beans-dev.json");
						}
						if (!ArgsUtils.exists(newArgs, "db")) {
							newArgs = ArraysUtil.append(newArgs, "--db");
							newArgs = ArraysUtil.append(newArgs, "mapdb");
						}
						manager.run(newArgs);
					} catch (Exception e) {
						LibsLogger.error(BeansBulkMain.class, "Cannot start BEANS server", e);
					}
				}
			}.start();
			
			// start web interface
//			new Thread(){ 
//				@Override
//				public void run() {
//					try {
//						// default: web --start
//						String[] newArgs = args2;
//						newArgs = ArraysUtil.insert(newArgs, new String[]{"web"}, 0);
//						if (!ArgsUtils.exists(newArgs, "start")) {
//							newArgs = ArraysUtil.append(newArgs, "--start");
//						}
//						manager.run(newArgs);
//					} catch (Throwable e) {
//						LibsLogger.error(BeansBulkMain.class, "Cannot start BEANS web client", e);
//					}
//				}
//			}.start();
			
		} else {
			
			try {
				CLICommand cmd = manager.run(args2);
				
//				if (cmd instanceof)
				
				// ApiCallCommand.getBeansAPI().getCall("api/table/data", null); dziala
				
				
//				FileOutputStream fos = new FileOutputStream(new File("aa")); 
//				
//				((ApiCallCommand) cmd).runStream().write(System.out);//fos);
				
//				byte[] bytes = buffer.toByteArray();
//				InputStream inputStream = new ByteArrayInputStream(bytes);
//				
//				FileUtils.saveToFile(inputStream, new File("aa"));

				
//				FileOutputStream fos = new FileOutputStream("");
//				fos.
//				
//				System.out.
//				
//				FileWriter.
//				FileUtils.saveToFile(((ApiCallCommand) cmd).runStream(), new File("aa"));
//				
				if (cmd instanceof ApiCallCommand) {
					ApiCallCommand apiCall = (ApiCallCommand) cmd;
					if (apiCall.getApiCall() == null) {
						// do nothing
					} else if (apiCall != null
							&& apiCall.getApiCall().getReturnValue() == APIReturnType.OUTPUTSTREAM) {
//						apiCall.runStream().write(System.out);//fos);
						FileOutputStream fos = new FileOutputStream(new File("out")); 
						apiCall.runStream().write(fos);
					} else
						LibsLogger.info(BeansBulkMain.class,
								JsonUtils.objectToStringPretty(((ApiCallCommand) cmd).getResponse().getAsJson()));
				}
			} catch (Exception e) {
				LibsLogger.error(BeansBulkMain.class, "Command failed: ", e);
				
				if (args2 == null || args2.length == 0)
					manager.printHelp(false);
			}
			
			System.exit(1);
		}
	}
}
