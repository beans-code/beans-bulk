package net.beanscode.cli.bulk.server;

import java.io.IOException;

import net.beanscode.rest.BeansServerMain;
import net.hypki.libs5.cli.noninteractivecli.CLICommand;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.cli.noninteractivecli.CliArg;

public class Server extends CLICommand {
	
	@CliArg(name = "port",
			description = "Http port to start BEANS REST server",
			required = false)
	private int port = 25000;

	public Server() {
		
	}

	@Override
	public String getCLICommandName() {
		return "server";
	}

	@Override
	public String getCLICommandDescription() {
		return "Starts BEANS REST server";
	}

	@Override
	protected CLICommand doCommand() throws CLIException, IOException {
		startRestServer();
		
		return this;
	}
	
	public void run() throws CLIException, IOException {
		super.run(null);
	}

	private void startRestServer() throws IOException {
		try {
			BeansServerMain.main(getArgs());
		} catch (Exception e) {
			throw new IOException("Cannot start BEANS REST server", e);
		}
	}
}
